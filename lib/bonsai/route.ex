defmodule Bonsai.Route do
  def identity(input_filename) do
    input_filename
  end

  def set_exeption(input_filename, new_extension) do
    [basename, _old_extension] = String.split(input_filename, ".")
    "#{basename}.#{new_extension}"
  end
end
