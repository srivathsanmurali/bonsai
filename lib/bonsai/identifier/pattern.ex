defmodule Bonsai.Identifier.Pattern do
  alias Bonsai.Identifier
  
  @type t :: [Bonsai.Identifier]

  @spec from_list([Identifier]) :: Pattern
  def from_list(list_of_idetifiers), do: list_of_idetifiers

  @spec from_glob(String, String) :: Pattern
  def from_glob(glob, root_path) do
    Path.join(root_path, glob)
    |> Path.wildcard()
    |> Enum.map(&Path.relative_to(&1, root_path))
    |> Enum.map(&Identifier.from_file_path/1)
  end
end
