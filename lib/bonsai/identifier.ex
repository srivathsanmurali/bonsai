defmodule Bonsai.Identifier do
  defstruct path: nil

  @type t :: {
          path :: String.t()
        }

  def from_file_path(file_path) do
    path =
      file_path
      |> String.split("/")
      |> Enum.filter(fn fp -> fp != "" end)
      |> Enum.join("/")

    %__MODULE__{path: path}
  end

  @spec to_file_path(Identifier) :: String
  def to_file_path(%__MODULE__{path: path}), do: path
end
