defmodule Bonsai.Item do
  defstruct identifier: nil,
            body: nil

  @type t :: {
          identifier :: Bonsai.Identifier,
          body :: any
        }

  @doc """
  Create an item from identifier and root paths
  """
  @spec item(Bonsai.Identifier, any) :: __MODULE__
  def item(identifier, body \\ nil),
    do: %__MODULE__{
      identifier: identifier,
      body: body
    }

  @doc """
  sets the body of the item
  """
  @spec set_body(__MODULE__, any) :: Item
  def set_body(item, body), do: %__MODULE__{item | body: body}

  @doc """
  fmap applies function on body of item.
  """
  @spec fmap(Item, (Item -> Item)) :: Item
  def fmap(%__MODULE__{body: body} = item, func) do
    %__MODULE__{item | body: func.(body)}
  end
end
